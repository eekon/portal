-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `portal`;
CREATE DATABASE `portal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `portal`;

CREATE TABLE `portal_limit` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长id',
  `user` varchar(30) NOT NULL COMMENT '用户名，如果不指定。就是全局用户',
  `input_basic_rate` int(11) NOT NULL,
  `input_average_rate` int(11) NOT NULL,
  `input_peak_rate` int(11) NOT NULL,
  `output_basic_rate` int(11) NOT NULL,
  `output_average_rate` int(11) NOT NULL,
  `output_peak_rate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `portal_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长id',
  `user` varchar(30) NOT NULL COMMENT '用户名',
  `userip` varchar(20) NOT NULL COMMENT '用户ip地址',
  `auth_type` varchar(10) NOT NULL COMMENT '认证类型,portal?mac认证',
  `calling_station_id` varchar(20) NOT NULL COMMENT '用户端的mac地址',
  `called_station_id` varchar(32) NOT NULL COMMENT 'CalledStationId',
  `acct_session_id` varchar(50) NOT NULL COMMENT '会话id',
  `us` tinyint(4) NOT NULL COMMENT '是否是内部帐户',
  `created` bigint(20) NOT NULL COMMENT '创建这条数据的北京时间',
  `updated` bigint(20) NOT NULL COMMENT '这个会话结束的北京时间',
  `reason` varchar(50) NOT NULL COMMENT '什么原因下线的',
  PRIMARY KEY (`id`),
  UNIQUE KEY `acct_session_id` (`acct_session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `portal_mac` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长id',
  `mac` varchar(14) NOT NULL COMMENT 'mac地址',
  `user` varchar(30) NOT NULL COMMENT '用户名',
  `userip` varchar(16) NOT NULL COMMENT '用户ip地址',
  `us` tinyint(4) NOT NULL COMMENT '是否是内部帐户',
  `created` bigint(20) NOT NULL COMMENT '插入数据库的时间',
  `mark` varchar(30) NOT NULL DEFAULT '' COMMENT '备注别太长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mac` (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `portal_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长id',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '时间策略',
  `start` bigint(20) NOT NULL COMMENT '起始时间',
  `end` bigint(20) NOT NULL COMMENT '结束时间',
  `mark` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `portal_policy` (`id`, `name`, `start`, `end`, `mark`) VALUES
(3,	'商务洽谈',	1688619600,	1688623140,	'也不知道过来干嘛21'),
(9,	'过来面试',	1685289600,	1685375940,	'过来面试。。。。3')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `name` = VALUES(`name`), `start` = VALUES(`start`), `end` = VALUES(`end`), `mark` = VALUES(`mark`);

CREATE TABLE `portal_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长id',
  `acct_session_id` varchar(50) NOT NULL COMMENT '会话id',
  `user` varchar(30) NOT NULL COMMENT 'portal认证这个会存在,mac认证是mac地址',
  `name` varchar(30) NOT NULL COMMENT 'name,只在mac认证的时候。被赋值',
  `userip` varchar(16) NOT NULL COMMENT '用户端的ip地址',
  `calling_station_id` varchar(20) NOT NULL COMMENT '用户端的mac地址',
  `nasip` varchar(16) NOT NULL COMMENT 'nasip地址',
  `called_station_id` varchar(32) NOT NULL COMMENT 'CalledStationId',
  `auth_type` varchar(10) NOT NULL COMMENT '认证类型,portal?mac认证',
  `us` tinyint(4) NOT NULL COMMENT '是否是公司员工，1为真，0为假',
  `created` bigint(20) NOT NULL COMMENT '创建这条数据的北京时间',
  `updated` bigint(20) NOT NULL COMMENT '更新这条数据的北京时间，心跳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `portal_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cn` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '登陆名',
  `sn` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '中文名',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '手机号',
  `policy` int(11) NOT NULL COMMENT '策略id',
  `mark` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQE_portal_user_username` (`cn`),
  KEY `policy` (`policy`),
  CONSTRAINT `portal_user_ibfk_3` FOREIGN KEY (`policy`) REFERENCES `portal_policy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `portal_user` (`id`, `cn`, `sn`, `password`, `phone`, `policy`, `mark`) VALUES
(2,	'zhangsan',	'张某',	'123456',	'88888888888',	3,	'aaabbb'),
(3,	'lisi',	'李某',	'yHwn3CaKYT',	'11111111111',	9,	'bbbbc'),
(5,	'aaa',	'周某',	'111111',	'18510005758',	3,	'随便了'),
(6,	'aa',	'张某1',	'xSTWSDr3',	'11111111111',	3,	'')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `cn` = VALUES(`cn`), `sn` = VALUES(`sn`), `password` = VALUES(`password`), `phone` = VALUES(`phone`), `policy` = VALUES(`policy`), `mark` = VALUES(`mark`);

-- 2023-07-06 07:58:38
